document.addEventListener("DOMContentLoaded", function(e) {
    let peer_id;        /* other peer id */
    let username;       /* your username */
    let conn;           /* two way connection between peers */
    
    let localStream = null;             /* local video and audio stream */
    let remoteCamera = null;
    
    let localScreenStream = null;       /* screen stream of a localuser */
    let remoteScreen = null

    /**
     * Creating peer object - specifing used configuration and ICE servers
     */
    let peer = new Peer({
        //host: "localhost",
        //host: "192.168.100.170",
        //host: "10.10.251.91",
        //host: "10.0.0.68",
        host: "https://streaming-peer-server.cfapps.io/",
        port: 9000,
        path: '/peerjs',
        debug: 3,
        config: {
            'iceServers': [
                { url: 'stun:iphone-stun.strato-iphone.de:3478' },
                { url: 'stun:stun.l.google.com:19302' },
                { url: 'stun:stun1.l.google.com:19302' },
                { url: 'stun:stun2.l.google.com:19302' },
                { url: 'stun:stun3.l.google.com:19302' },
                { url: 'stun:stun4.l.google.com:19302' },
                { url: 'stun:stun.ekiga.net' },
                { url: 'stun:stun.ideasip.com' },
                { url: 'stun:stun.rixtelecom.se' },
                { url: 'stun:stun.schlund.de' },
                { url: 'stun:stun.stunprotocol.org:3478' },
                { url: 'stun:stun.voiparound.com' },
                { url: 'stun:stun.voipbuster.com' },
                { url: 'stun:stun.voipstunt.com' },
                { url: 'stun:stun.voxgratia.org' },
                
                { url: 'stun:numb.viagenie.ca:3478' },
                { url: 'stun:s1.taraba.net:3478' },
                { url: 'stun:s2.taraba.net:3478' },
                { url: 'stun:stun.12connect.com:3478' },
                { url: 'stun:stun.12voip.com:3478' },
                { url: 'stun:stun.1und1.de:3478' },
                { url: 'stun:stun.2talk.co.nz:3478' },
                { url: 'stun:stun.2talk.com:3478' },
                { url: 'stun:stun.3clogic.com:3478' },
                { url: 'stun:stun.3cx.com:3478' },
                { url: 'stun:stun.a-mm.tv:3478' },
                { url: 'stun:stun.aa.net.uk:3478' },
                { url: 'stun:stun.acrobits.cz:3478' },
                { url: 'stun:stun.actionvoip.com:3478' },
                { url: 'stun:stun.advfn.com:3478' },
                { url: 'stun:stun.aeta-audio.com:3478' },
                { url: 'stun:stun.aeta.com:3478' },
                { url: 'stun:stun.alltel.com.au:3478' },
                { url: 'stun:stun.altar.com.pl:3478' },
                { url: 'stun:stun.annatel.net:3478' },
                { url: 'stun:stun.antisip.com:3478' },
                { url: 'stun:stun.arbuz.ru:3478' },
                { url: 'stun:stun.avigora.com:3478' },
                { url: 'stun:stun.avigora.fr:3478' },
                { url: 'stun:stun.awa-shima.com:3478' },
                { url: 'stun:stun.awt.be:3478' },
                { url: 'stun:stun.b2b2c.ca:3478' },
                { url: 'stun:stun.bahnhof.net:3478' },
                { url: 'stun:stun.barracuda.com:3478' },
                { url: 'stun:stun.bluesip.net:3478' },
                { url: 'stun:stun.bmwgs.cz:3478' },
                { url: 'stun:stun.botonakis.com:3478' },
                { url: 'stun:stun.budgetphone.nl:3478' },
                { url: 'stun:stun.budgetsip.com:3478' },
                { url: 'stun:stun.cablenet-as.net:3478' },
                { url: 'stun:stun.callromania.ro:3478' },
                { url: 'stun:stun.callwithus.com:3478' },
                { url: 'stun:stun.cbsys.net:3478' },
                { url: 'stun:stun.chathelp.ru:3478' },
                { url: 'stun:stun.cheapvoip.com:3478' },
                { url: 'stun:stun.ciktel.com:3478' },
                { url: 'stun:stun.cloopen.com:3478' },
                { url: 'stun:stun.colouredlines.com.au:3478' },
                { url: 'stun:stun.comfi.com:3478' },
                { url: 'stun:stun.commpeak.com:3478' },
                { url: 'stun:stun.comtube.com:3478' },
                { url: 'stun:stun.comtube.ru:3478' },
                { url: 'stun:stun.cope.es:3478' },
                { url: 'stun:stun.counterpath.com:3478' },
                { url: 'stun:stun.counterpath.net:3478' },
                { url: 'stun:stun.cryptonit.net:3478' },
                { url: 'stun:stun.darioflaccovio.it:3478' },
                { url: 'stun:stun.datamanagement.it:3478' },
                { url: 'stun:stun.dcalling.de:3478' },
                { url: 'stun:stun.decanet.fr:3478' },
                { url: 'stun:stun.demos.ru:3478' },
                { url: 'stun:stun.develz.org:3478' },
                { url: 'stun:stun.dingaling.ca:3478' },
                { url: 'stun:stun.doublerobotics.com:3478' },
                { url: 'stun:stun.drogon.net:3478' },
                { url: 'stun:stun.duocom.es:3478' },
                { url: 'stun:stun.dus.net:3478' },
                { url: 'stun:stun.e-fon.ch:3478' },
                { url: 'stun:stun.easybell.de:3478' },
                { url: 'stun:stun.easycall.pl:3478' },
                { url: 'stun:stun.easyvoip.com:3478' },
                { url: 'stun:stun.efficace-factory.com:3478' },
                { url: 'stun:stun.einsundeins.com:3478' },
                { url: 'stun:stun.einsundeins.de:3478' },
                { url: 'stun:stun.ekiga.net:3478' },
                { url: 'stun:stun.epygi.com:3478' },
                { url: 'stun:stun.etoilediese.fr:3478' },
                { url: 'stun:stun.eyeball.com:3478' },
                { url: 'stun:stun.faktortel.com.au:3478' },
                { url: 'stun:stun.freecall.com:3478' },
                { url: 'stun:stun.freeswitch.org:3478' },
                { url: 'stun:stun.freevoipdeal.com:3478' },
                { url: 'stun:stun.fuzemeeting.com:3478' },
                { url: 'stun:stun.gmx.de:3478' },
                { url: 'stun:stun.gmx.net:3478' },
                { url: 'stun:stun.gradwell.com:3478' },
                { url: 'stun:stun.halonet.pl:3478' },
                { url: 'stun:stun.hellonanu.com:3478' },
                { url: 'stun:stun.hoiio.com:3478' },
                { url: 'stun:stun.hosteurope.de:3478' },
                { url: 'stun:stun.ideasip.com:3478' },
                { url: 'stun:stun.imesh.com:3478' },
                { url: 'stun:stun.infra.net:3478' },
                { url: 'stun:stun.internetcalls.com:3478' },
                { url: 'stun:stun.intervoip.com:3478' },
                { url: 'stun:stun.ipcomms.net:3478' },
                { url: 'stun:stun.ipfire.org:3478' },
                { url: 'stun:stun.ippi.fr:3478' },
                { url: 'stun:stun.ipshka.com:3478' },
                { url: 'stun:stun.iptel.org:3478' },
                { url: 'stun:stun.irian.at:3478' },
                { url: 'stun:stun.it1.hr:3478' },
                { url: 'stun:stun.ivao.aero:3478' },
                { url: 'stun:stun.jappix.com:3478' },
                { url: 'stun:stun.jumblo.com:3478' },
                { url: 'stun:stun.justvoip.com:3478' },
                { url: 'stun:stun.kanet.ru:3478' },
                { url: 'stun:stun.kiwilink.co.nz:3478' },
                { url: 'stun:stun.kundenserver.de:3478' },
                { url: 'stun:stun.l.google.com:19302' },
                { url: 'stun:stun.linea7.net:3478' },
                { url: 'stun:stun.linphone.org:3478' },
                { url: 'stun:stun.liveo.fr:3478' },
                { url: 'stun:stun.lowratevoip.com:3478' },
                { url: 'stun:stun.lugosoft.com:3478' },
                { url: 'stun:stun.lundimatin.fr:3478' },
                { url: 'stun:stun.magnet.ie:3478' },
                { url: 'stun:stun.manle.com:3478' },
                { url: 'stun:stun.mgn.ru:3478' },
                { url: 'stun:stun.mit.de:3478' },
                { url: 'stun:stun.mitake.com.tw:3478' },
                { url: 'stun:stun.miwifi.com:3478' },
                { url: 'stun:stun.modulus.gr:3478' },
                { url: 'stun:stun.mozcom.com:3478' },
                { url: 'stun:stun.myvoiptraffic.com:3478' },
                { url: 'stun:stun.mywatson.it:3478' },
                { url: 'stun:stun.nas.net:3478' },
                { url: 'stun:stun.neotel.co.za:3478' },
                { url: 'stun:stun.netappel.com:3478' },
                { url: 'stun:stun.netappel.fr:3478' },
                { url: 'stun:stun.netgsm.com.tr:3478' },
                { url: 'stun:stun.nfon.net:3478' },
                { url: 'stun:stun.noblogs.org:3478' },
                { url: 'stun:stun.noc.ams-ix.net:3478' },
                { url: 'stun:stun.node4.co.uk:3478' },
                { url: 'stun:stun.nonoh.net:3478' },
                { url: 'stun:stun.nottingham.ac.uk:3478' },
                { url: 'stun:stun.nova.is:3478' },
                { url: 'stun:stun.nventure.com:3478' },
                { url: 'stun:stun.on.net.mk:3478' },
                { url: 'stun:stun.ooma.com:3478' },
                { url: 'stun:stun.ooonet.ru:3478' },
                { url: 'stun:stun.oriontelekom.rs:3478' },
                { url: 'stun:stun.outland-net.de:3478' },
                { url: 'stun:stun.ozekiphone.com:3478' },
                { url: 'stun:stun.patlive.com:3478' },
                { url: 'stun:stun.personal-voip.de:3478' },
                { url: 'stun:stun.petcube.com:3478' },
                { url: 'stun:stun.phone.com:3478' },
                { url: 'stun:stun.phoneserve.com:3478' },
                { url: 'stun:stun.pjsip.org:3478' },
                { url: 'stun:stun.poivy.com:3478' },
                { url: 'stun:stun.powerpbx.org:3478' },
                { url: 'stun:stun.powervoip.com:3478' },
                { url: 'stun:stun.ppdi.com:3478' },
                { url: 'stun:stun.prizee.com:3478' },
                { url: 'stun:stun.qq.com:3478' },
                { url: 'stun:stun.qvod.com:3478' },
                { url: 'stun:stun.rackco.com:3478' },
                { url: 'stun:stun.rapidnet.de:3478' },
                { url: 'stun:stun.rb-net.com:3478' },
                { url: 'stun:stun.refint.net:3478' },
                { url: 'stun:stun.remote-learner.net:3478' },
                { url: 'stun:stun.rixtelecom.se:3478' },
                { url: 'stun:stun.rockenstein.de:3478' },
                { url: 'stun:stun.rolmail.net:3478' },
                { url: 'stun:stun.rounds.com:3478' },
                { url: 'stun:stun.rynga.com:3478' },
                { url: 'stun:stun.samsungsmartcam.com:3478' },
                { url: 'stun:stun.schlund.de:3478' },
                { url: 'stun:stun.services.mozilla.com:3478' },
                { url: 'stun:stun.sigmavoip.com:3478' },
                { url: 'stun:stun.sip.us:3478' },
                { url: 'stun:stun.sipdiscount.com:3478' },
                { url: 'stun:stun.sipgate.net:10000' },
                { url: 'stun:stun.sipgate.net:3478' },
                { url: 'stun:stun.siplogin.de:3478' },
                { url: 'stun:stun.sipnet.net:3478' },
                { url: 'stun:stun.sipnet.ru:3478' },
                { url: 'stun:stun.siportal.it:3478' },
                { url: 'stun:stun.sippeer.dk:3478' },
                { url: 'stun:stun.siptraffic.com:3478' },
                { url: 'stun:stun.skylink.ru:3478' },
                { url: 'stun:stun.sma.de:3478' },
                { url: 'stun:stun.smartvoip.com:3478' },
                { url: 'stun:stun.smsdiscount.com:3478' },
                { url: 'stun:stun.snafu.de:3478' },
                { url: 'stun:stun.softjoys.com:3478' },
                { url: 'stun:stun.solcon.nl:3478' },
                { url: 'stun:stun.solnet.ch:3478' },
                { url: 'stun:stun.sonetel.com:3478' },
                { url: 'stun:stun.sonetel.net:3478' },
                { url: 'stun:stun.sovtest.ru:3478' },
                { url: 'stun:stun.speedy.com.ar:3478' },
                { url: 'stun:stun.spokn.com:3478' },
                { url: 'stun:stun.srce.hr:3478' },
                { url: 'stun:stun.ssl7.net:3478' },
                { url: 'stun:stun.stunprotocol.org:3478' },
                { url: 'stun:stun.symform.com:3478' },
                { url: 'stun:stun.symplicity.com:3478' },
                { url: 'stun:stun.sysadminman.net:3478' },
                { url: 'stun:stun.t-online.de:3478' },
                { url: 'stun:stun.tagan.ru:3478' },
                { url: 'stun:stun.tatneft.ru:3478' },
                { url: 'stun:stun.teachercreated.com:3478' },
                { url: 'stun:stun.tel.lu:3478' },
                { url: 'stun:stun.telbo.com:3478' },
                { url: 'stun:stun.telefacil.com:3478' },
                { url: 'stun:stun.tis-dialog.ru:3478' },
                { url: 'stun:stun.tng.de:3478' },
                { url: 'stun:stun.twt.it:3478' },
                { url: 'stun:stun.u-blox.com:3478' },
                { url: 'stun:stun.ucallweconn.net:3478' },
                { url: 'stun:stun.ucsb.edu:3478' },
                { url: 'stun:stun.ucw.cz:3478' },
                { url: 'stun:stun.uls.co.za:3478' },
                { url: 'stun:stun.unseen.is:3478' },
                { url: 'stun:stun.usfamily.net:3478' },
                { url: 'stun:stun.veoh.com:3478' },
                { url: 'stun:stun.vidyo.com:3478' },
                { url: 'stun:stun.vipgroup.net:3478' },
                { url: 'stun:stun.virtual-call.com:3478' },
                { url: 'stun:stun.viva.gr:3478' },
                { url: 'stun:stun.vivox.com:3478' },
                { url: 'stun:stun.vline.com:3478' },
                { url: 'stun:stun.vo.lu:3478' },
                { url: 'stun:stun.vodafone.ro:3478' },
                { url: 'stun:stun.voicetrading.com:3478' },
                { url: 'stun:stun.voip.aebc.com:3478' },
                { url: 'stun:stun.voip.blackberry.com:3478' },
                { url: 'stun:stun.voip.eutelia.it:3478' },
                { url: 'stun:stun.voiparound.com:3478' },
                { url: 'stun:stun.voipblast.com:3478' },
                { url: 'stun:stun.voipbuster.com:3478' },
                { url: 'stun:stun.voipbusterpro.com:3478' },
                { url: 'stun:stun.voipcheap.co.uk:3478' },
                { url: 'stun:stun.voipcheap.com:3478' },
                { url: 'stun:stun.voipfibre.com:3478' },
                { url: 'stun:stun.voipgain.com:3478' },
                { url: 'stun:stun.voipgate.com:3478' },
                { url: 'stun:stun.voipinfocenter.com:3478' },
                { url: 'stun:stun.voipplanet.nl:3478' },
                { url: 'stun:stun.voippro.com:3478' },
                { url: 'stun:stun.voipraider.com:3478' },
                { url: 'stun:stun.voipstunt.com:3478' },
                { url: 'stun:stun.voipwise.com:3478' },
                { url: 'stun:stun.voipzoom.com:3478' },
                { url: 'stun:stun.vopium.com:3478' },
                { url: 'stun:stun.voxgratia.org:3478' },
                { url: 'stun:stun.voxox.com:3478' },
                { url: 'stun:stun.voys.nl:3478' },
                { url: 'stun:stun.voztele.com:3478' },
                { url: 'stun:stun.vyke.com:3478' },
                { url: 'stun:stun.webcalldirect.com:3478' },
                { url: 'stun:stun.whoi.edu:3478' },
                { url: 'stun:stun.wifirst.net:3478' },
                { url: 'stun:stun.wwdl.net:3478' },
                { url: 'stun:stun.xs4all.nl:3478' },
                { url: 'stun:stun.xtratelecom.es:3478' },
                { url: 'stun:stun.yesss.at:3478' },
                { url: 'stun:stun.zadarma.com:3478' },
                { url: 'stun:stun.zadv.com:3478' },
                { url: 'stun:stun.zoiper.com:3478' },
                { url: 'stun:stun1.faktortel.com.au:3478' },
                { url: 'stun:stun1.l.google.com:19302' },
                { url: 'stun:stun1.voiceeclipse.net:3478' },
                { url: 'stun:stun2.l.google.com:19302' },
                { url: 'stun:stun3.l.google.com:19302' },
                { url: 'stun:stun4.l.google.com:19302' },
                { url: 'stun:stunserver.org:3478' },
                
                {
                    url: 'turn:numb.viagenie.ca',
                    credential: 'muazkh',
                    username: 'webrtc@live.com'
                },
                
                {
                    url: 'turn:192.158.29.39:3478?transport=udp',
                    credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
                    username: '28224511:1379330808'
                },
                {
                    url: 'turn:192.158.29.39:3478?transport=tcp',
                    credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
                    username: '28224511:1379330808'
                },
                {
                    url: 'turn:turn.bistri.com:80',
                    credential: 'homeo',
                    username: 'homeo'
                 },
                 {
                    url: 'turn:turn.anyfirewall.com:443?transport=tcp',
                    credential: 'webrtc',
                    username: 'webrtc'
                }
            ]
        }
    });
    
    /**************************************************************************************/
    /*************************************** EVENTS ***************************************/
    /**************************************************************************************/
    
    /**
    * "Initialization successful" Event
    *
    * Show ID of current peer on screen
    */
    peer.on('open', function () {
        document.getElementById("peer-id-label").innerHTML = peer.id;
    });

    /**
    * "Someone connected" Event
    */
    peer.on('connection', function (connection) {
        conn = connection;
        peer_id = connection.peer;

        //Use the handleMessage to callback when a message comes in
        conn.on('data', handleMessage);

        //Hide peer_id field and set the incoming peer id as value
        document.getElementById("peer_id").className += " hidden";
        document.getElementById("peer_id").value = peer_id;
        document.getElementById("connected_peer").innerHTML = connection.metadata.username;
        
        if(connection.metadata.reply){
            username = document.getElementById("name").value;

            console.log("connecting with:" + peer_id);

            connectToPeer(peer_id, username, false);
        }
    });

    /**
    * "On Error" Event
    */
    peer.on('error', function(err){
        alert("An error ocurred with peer: " + err);
        console.error(err);
    });

    /**
     * "receive call" Event
     */
    peer.on('call', function (call) {
        //let acceptsCall = confirm("Videocall incoming, do you want to accept it ?");

        //if(acceptsCall || true){
            // Answer the call with your own video/audio stream                                 //TODO
            
            console.log("*_*_*_*_*_**_**__**__*_**__**_ call.metadata = " + call.type);
        
            if(call.metadata.typeOfMsg == "camera") {
                call.answer(localStream);
                
                // Receive data
                call.on('stream', function (stream) {
                    // Store a global reference of the other user stream
                    remoteCamera = stream;

                    // Display the stream of the other user in the peer-camera video element !
                    bindStreamToVideoElement(stream, 'remote-camera');
                });

                // Handle when the call finishes
                call.on('close', function(){
                    alert("The videocall has finished");
                });
            } else {        //SCREEN
                call.answer(localScreenStream);
                
                call.on('stream', function(stream){
                    remoteScreen = stream;
                    
                    bindStreamToVideoElement(stream, 'screen-remote');
                });
                
                call.on('close', function(){
                    alert("The videocall has finished");
                });
            }

            // use call.close() to finish a call
        //}else{
        //    console.log("Call denied !");
        //}
    });
    
    /**************************************************************************************/
    /********************************** BASIC FUNCTIONS ***********************************/
    /**************************************************************************************/
    
    /**
     * Requesting access to camera and microphone
     *
     * @param {Object} callbacks
     */
    function requestLocalVideo(callbacks) {
        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

        // Request access to audio and video
        navigator.getUserMedia({ 
                audio: true, 
                video: true 
            }, callbacks.success , callbacks.error
        );
    }
    
    /**
    * Requesting local stream of users screen
    */
    function requestLocalScreenStream(){
        if(navigator.mediaDevices.getDisplayMedia){
            navigator.mediaDevices.getDisplayMedia({ 
                video: {  
                    mediaSource: "screen",
                    //width: { max: '1920' },
                    //height: { max: '1080' },
                    width: {max: '4096'},
                    height: {max: '2048'},
                    frameRate: { max: '30' }
                }  
            }).then(function(stream){
                    localScreenStream = stream;
            });
        } else if(navigator.getDisplayMedia){
            navigator.shimGetDisplayMedia({
                video: {
                    meidaSource: "screen",
                    //width: { max: '1920' },
                    //height: { max: '1080' },
                    width: {max: '4096'},
                    height: {max: '2048'},
                    frameRate: { max: '30' }
                }
            }).then(function(stream){
                    localScreenStream = stream;
            });
        }
    }
    
    /**
     * Directing stream to desired video element
     *
     * @param {*} stream
     * @param {*} element_id
     */
    function bindStreamToVideoElement(stream, element_id) {
        //Retrieve the video element
        let video = document.getElementById(element_id);
        
        //Setting the given stream as the video source
		console.log("PeerJS: stream" + stream + " element id " + element_id);
		let sstream = new MediaStream(stream);
		video.srcObject = sstream;
    }

    /**
    * Connects to the other peer
    *
    * @param {string} peerID others peer id
    * @param {string} yourUsername your username
    * @param {boolean} reply if true creating 2 way connection
    */
    function connectToPeer(peerID, yourUsername, reply){
        if (peerID) {
            conn = peer.connect(peerID, {
                metadata: {
                    'username': yourUsername,
                    'reply':   reply
                }
            });

            conn.on('data', handleMessage);
        }else{
            alert("You need to provide a peer to connect with !");
            return false;
        }

        document.getElementById("chat").className = "";
        document.getElementById("connection-form").className += " hidden";
    }
    
    /**
     * Adds received message to the chat
     *
     * @param {Object} data
     */
    function handleMessage(data) {
        let orientation = "text-left";

        // If the message is yours it should be on right side of chat window
        if(data.from == username){
            orientation = "text-right"
        }

        let messageHTML =  '<a href="javascript:void(0);" class="list-group-item' + orientation + '">';
                messageHTML += '<h4 class="list-group-item-heading">'+ data.from +'</h4>';
                messageHTML += '<p class="list-group-item-text">'+ data.text +'</p>';
            messageHTML += '</a>';

        document.getElementById("messages").innerHTML += messageHTML;
    }

    /**************************************************************************************/
    /*************************  MAPPING FUNCTIONS TO HTML ELEMENTS ************************/
    /**************************************************************************************/
    
    /**
    * Sends a message to other user 
    */
    document.getElementById("send-message").addEventListener("click", function(){
        let text = document.getElementById("message").value;

        //Prepare the data to send
        let data = {
            from: username,
            text: text
        };

        //Send the message with Peer
        conn.send(data);

        //Add the message to the chat window
        handleMessage(data);

        document.getElementById("message").value = "";
    }, false);

    /**
     *  Requests a videocall from the other user
     */
    document.getElementById("call").addEventListener("click", function(){
        console.log('Calling to ' + peer_id);
        console.log(peer);
        
        let callCamera = peer.call(peer_id, localStream, {
            metadata: {
                'typeOfMsg': "camera"
            }
        });
        
        let callScreen = peer.call(peer_id, localScreenStream, {
            metadata: {
                'typeOfMsg': "screen"
            }
        })
        
        callCamera.on('stream', function (stream) {
            bindStreamToVideoElement(stream, 'remote-camera');
        });
        
        callScreen.on('stream', function(stream){
            bindStreamToVideoElement(stream, 'screen-remote');
        })
    }, false);

    /**
     * Connects two users
     */
    document.getElementById("connect-to-peer-btn").addEventListener("click", function(){
        username = document.getElementById("name").value;
        peer_id = document.getElementById("peer_id").value;

        connectToPeer(peer_id, username, true);
    }, false);

    /**************************************************************************************/
    /*******************************  REQUEST YOUR OWN VIDEO ******************************/
    /**************************************************************************************/
    
    /**
     * Request your own video
     */
    requestLocalVideo({
        success: function(stream){
            localStream = stream;
            bindStreamToVideoElement(stream, 'local-camera');
        },
        error: function(err){
            alert("Cannot get access to your camera and video !");
            console.error(err);
        }
    })
        
    requestLocalScreenStream();

    
    
}, false);